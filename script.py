import tempfile
import time
import requests
import sh
import shutil

storage_path = tempfile.mkdtemp()

gcloud = sh.Command('dev_appserver.py')(
    'app.yaml',
    '--skip_sdk_update_check=yes',
    '--admin_host=0.0.0.0',
    '--admin_port=8000',
    '--api_port=8080',
    '--clear_datastore=True',
    '--datastore_consistency_policy=consistent',
    '--require_indexes=True',
    '--storage_path=' + storage_path,
    _bg=True
)

try:
    # Repeatedly call the warmup until a OK is returned. This means the
    # warmup has finished. Maximal retries: 600, one per tenth second.
    for dummy in range(600):
        try:
            response = requests.get('http://localhost:8080/_ah/warmup')
        except requests.exceptions.ConnectionError:
            time.sleep(0.1)
        else:
            if response.status_code == 200:
                break
    else:
        raise AssertionError('Failed to start gcloud')

    yield
finally:
    # Now the gcloud process probably still exists, fetch its children.
    try:
        p = psutil.Process(gcloud.pid)
        children = p.children(recursive=True)
    except psutil.NoSuchProcess:
        children = []

    # Killing parent before children to prevent respawning children.
    try:
        gcloud.terminate()
    except AttributeError:
        pass

    for child in children:
        # If the process is not running, an OSError is raised.
        try:
            os.kill(child.pid, signal.SIGTERM)
            os.waitpid(child.pid, 0)
        except OSError:
            pass

    try:
        gcloud.wait()
    except (AttributeError, sh.SignalException):
        pass

    shutil.rmtree(storage_path)